/*
 * Frm_Main.java
 * Entry point of application and handles the GUI
 * Author: Phyo Myat Thu
 */


package com.ui;
import java.awt.EventQueue;

import javax.swing.JFileChooser;
import javax.swing.JFrame;

import com.pipes.UIToInputProcessorPipe;
import javax.swing.JTextArea;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.JLabel;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import java.awt.Color;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;

public class Frm_Main implements ActionListener {

	private static JFrame frmKeywordInContext;
	private JFileChooser chooser;
	private JLabel lblInputTitlesFilePath;
	private JLabel lblInputIgnoreWordsFilePath;
	private JLabel lblNumOfIgnoreWordsDisplay;
	private JButton btnProcessInput;
	private JButton btnSelectIgnoreWords;
	private JButton btnSelectTitles;
	private UIToInputProcessorPipe inputProcessorPipe;
	private JTextArea textAreaInputTitles;
	private JTextArea textAreaInputIgnoreWords;
	private static JTextArea textAreaResultOutput;
	private JLabel lblNumOfTitlesDisplay;
	private ArrayList<String> titleList;
	private ArrayList<String> ignoreList;
	public static ArrayList<String> finalOutput;
	private JLabel label;
	private static JLabel lblResultNumOfTitlesDisplay;
	private JScrollPane scrollPane_1;
	private JScrollPane scrollPane_2;
	private JPanel panel_1;
	private static JLabel lblStatusDisplay;
	private static long startTime;
	private JLabel lblTitlesFile;
	private JLabel lblIgnoreWordsFile;
	private JLabel lblOuputTitleFilePath;
	private JLabel lblOutputIgnoreWordFilePath;
	private JPanel panel_2;
	private JPanel panel_3;
	private static JButton btnSaveAs;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Frm_Main window = new Frm_Main();
					window.frmKeywordInContext.setLocationRelativeTo(null);
					window.frmKeywordInContext.setVisible(true);
					
					URL url = ClassLoader.getSystemResource("com/ui/kwic_logo.png");
					Toolkit kit = Toolkit.getDefaultToolkit();
					Image img = kit.createImage(url);
					window.frmKeywordInContext.setIconImage(img);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Frm_Main() {
		initialize();
		inputProcessorPipe = new UIToInputProcessorPipe("", "");
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmKeywordInContext = new JFrame();
		frmKeywordInContext.setTitle("Key Word In Context");
		frmKeywordInContext.setResizable(false);
		frmKeywordInContext.setBounds(100, 100, 800, 694);
		frmKeywordInContext.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmKeywordInContext.getContentPane().setLayout(null);
						
						JPanel panel = new JPanel();
						panel.setBorder(new LineBorder(new Color(0, 0, 0)));
						panel.setBounds(10, 11, 382, 625);
						frmKeywordInContext.getContentPane().add(panel);
						panel.setLayout(null);
								
								JLabel lblNewLabel_1 = new JLabel("Input File:");
								lblNewLabel_1.setBounds(10, 11, 71, 17);
								panel.add(lblNewLabel_1);
								lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 14));
										
										btnProcessInput = new JButton("Process Input");
										btnProcessInput.setEnabled(false);
										btnProcessInput.setBounds(119, 591, 150, 23);
										panel.add(btnProcessInput);
										
										JLabel lblTitles = new JLabel("Title(s):");
										lblTitles.setFont(new Font("Tahoma", Font.BOLD, 12));
										lblTitles.setBounds(10, 43, 54, 14);
										panel.add(lblTitles);
										
										JLabel lblIgnoreWords = new JLabel("Ignore Word(s):");
										lblIgnoreWords.setFont(new Font("Tahoma", Font.BOLD, 12));
										lblIgnoreWords.setBounds(10, 381, 110, 14);
										panel.add(lblIgnoreWords);
										
										JScrollPane scrollPane = new JScrollPane();
										scrollPane.setBounds(10, 110, 362, 260);
										panel.add(scrollPane);
										
										textAreaInputTitles = new JTextArea();
										scrollPane.setViewportView(textAreaInputTitles);
										textAreaInputTitles.setEditable(false);
										
										JLabel lblNumOfTitles = new JLabel("Total Numer of Title(s):");
										lblNumOfTitles.setFont(new Font("Tahoma", Font.BOLD, 11));
										lblNumOfTitles.setBounds(10, 95, 135, 14);
										panel.add(lblNumOfTitles);
										
										lblNumOfTitlesDisplay = new JLabel("0");
										lblNumOfTitlesDisplay.setBounds(145, 95, 141, 14);
										panel.add(lblNumOfTitlesDisplay);
										
										JLabel lblTotalNumerOf = new JLabel("Total Numer of Ignore Word(s):");
										lblTotalNumerOf.setFont(new Font("Tahoma", Font.BOLD, 11));
										lblTotalNumerOf.setBounds(10, 439, 186, 14);
										panel.add(lblTotalNumerOf);
										
										lblNumOfIgnoreWordsDisplay = new JLabel("0");
										lblNumOfIgnoreWordsDisplay.setBounds(196, 439, 90, 14);
										panel.add(lblNumOfIgnoreWordsDisplay);
										
										scrollPane_1 = new JScrollPane();
										scrollPane_1.setBounds(10, 453, 362, 134);
										panel.add(scrollPane_1);
										
										textAreaInputIgnoreWords = new JTextArea();
										scrollPane_1.setViewportView(textAreaInputIgnoreWords);
										textAreaInputIgnoreWords.setEditable(false);
										
										panel_2 = new JPanel();
										panel_2.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
										panel_2.setBounds(10, 59, 362, 31);
										panel.add(panel_2);
												panel_2.setLayout(null);
										
												
												lblInputTitlesFilePath = new JLabel("Not Specified");
												lblInputTitlesFilePath.setBounds(10, 9, 242, 14);
												panel_2.add(lblInputTitlesFilePath);
												lblInputTitlesFilePath.setFont(new Font("Tahoma", Font.ITALIC, 11));
												
												btnSelectTitles = new JButton("Choose...");
												btnSelectTitles.setBounds(262, 5, 90, 23);
												panel_2.add(btnSelectTitles);
												
												panel_3 = new JPanel();
												panel_3.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
												panel_3.setBounds(10, 400, 362, 31);
												panel.add(panel_3);
												panel_3.setLayout(null);
												
												lblInputIgnoreWordsFilePath = new JLabel("Not Specified");
												lblInputIgnoreWordsFilePath.setBounds(10, 9, 241, 14);
												panel_3.add(lblInputIgnoreWordsFilePath);
												lblInputIgnoreWordsFilePath.setFont(new Font("Tahoma", Font.ITALIC, 11));
												
												btnSelectIgnoreWords = new JButton("Select...");
												btnSelectIgnoreWords.setBounds(261, 5, 91, 23);
												panel_3.add(btnSelectIgnoreWords);
												btnSelectIgnoreWords.addActionListener(this);
												
								btnSelectTitles.addActionListener(this);
										
										JPanel pnlOutput = new JPanel();
										pnlOutput.setBorder(new LineBorder(new Color(0, 0, 0)));
										pnlOutput.setBounds(402, 11, 382, 625);
										frmKeywordInContext.getContentPane().add(pnlOutput);
										pnlOutput.setLayout(null);
										
										JLabel lblOutputResults = new JLabel("Output Results:");
										lblOutputResults.setFont(new Font("Tahoma", Font.BOLD, 14));
										lblOutputResults.setBounds(10, 11, 132, 17);
										pnlOutput.add(lblOutputResults);
										
										label = new JLabel("Total Numer of Title(s):");
										label.setHorizontalAlignment(SwingConstants.TRAILING);
										label.setFont(new Font("Tahoma", Font.BOLD, 11));
										label.setBounds(10, 72, 135, 14);
										pnlOutput.add(label);
										
										lblResultNumOfTitlesDisplay = new JLabel("0");
										lblResultNumOfTitlesDisplay.setBounds(147, 73, 143, 14);
										pnlOutput.add(lblResultNumOfTitlesDisplay);
										
										scrollPane_2 = new JScrollPane();
										scrollPane_2.setBounds(10, 97, 362, 491);
										pnlOutput.add(scrollPane_2);
										
										textAreaResultOutput = new JTextArea();
										scrollPane_2.setViewportView(textAreaResultOutput);
										textAreaResultOutput.setEditable(false);
										
										lblTitlesFile = new JLabel("Title(s) File:");
										lblTitlesFile.setHorizontalAlignment(SwingConstants.TRAILING);
										lblTitlesFile.setFont(new Font("Tahoma", Font.BOLD, 11));
										lblTitlesFile.setBounds(10, 34, 135, 14);
										pnlOutput.add(lblTitlesFile);
										
										lblIgnoreWordsFile = new JLabel("Ignore Word(s) File:");
										lblIgnoreWordsFile.setHorizontalAlignment(SwingConstants.TRAILING);
										lblIgnoreWordsFile.setFont(new Font("Tahoma", Font.BOLD, 11));
										lblIgnoreWordsFile.setBounds(10, 54, 135, 14);
										pnlOutput.add(lblIgnoreWordsFile);
										
										lblOuputTitleFilePath = new JLabel("Not Specified");
										lblOuputTitleFilePath.setFont(new Font("Tahoma", Font.PLAIN, 11));
										lblOuputTitleFilePath.setBounds(147, 34, 143, 14);
										pnlOutput.add(lblOuputTitleFilePath);
										
										lblOutputIgnoreWordFilePath = new JLabel("Not Specified");
										lblOutputIgnoreWordFilePath.setFont(new Font("Tahoma", Font.PLAIN, 11));
										lblOutputIgnoreWordFilePath.setBounds(147, 54, 143, 14);
										pnlOutput.add(lblOutputIgnoreWordFilePath);
										
										btnSaveAs = new JButton("Save As...");
										btnSaveAs.setEnabled(false);
										btnSaveAs.setBounds(270, 591, 102, 23);
										pnlOutput.add(btnSaveAs);
										
										panel_1 = new JPanel();
										panel_1.setBounds(0, 647, 650, 21);
										frmKeywordInContext.getContentPane().add(panel_1);
										panel_1.setLayout(null);
										
										lblStatusDisplay = new JLabel("-");
										lblStatusDisplay.setEnabled(false);
										lblStatusDisplay.setBounds(10, 0, 591, 14);
										panel_1.add(lblStatusDisplay);
								btnProcessInput.addActionListener(this);
								btnSaveAs.addActionListener(this);
	}
	
	/*
	 * Event listeners
	 */
	@Override
	
	public void actionPerformed(ActionEvent ae) {
	
        String action = ae.getActionCommand();

        if (action.equals("Choose...")) {
        	//load titles
        	loadTitlesFromFile();
        }
        else if (action.equals("Select...")) {
        	// load ignore word list
        	loadIgnoreWordsFromFile();
        }
        else if (action.equalsIgnoreCase("process input")) {
        	if(titleList.size() > 100000)
        	{
        		lblStatusDisplay.setText("Processing a large number of titles. Please be patient...");
        		lblStatusDisplay.repaint();
        	}else{
        		lblStatusDisplay.setText("Processing the titles. Will be completed in a short while...");
        		lblStatusDisplay.repaint();
        	}
        	// Perform circular shift
        	processInputs();
        }
        else if (action.equalsIgnoreCase("save as...")) {
        	// Save the output
        	saveAs();
        }
	}
	
	private void loadTitlesFromFile(){
		chooser = new JFileChooser();
		chooser.setCurrentDirectory(new File("."));
		FileNameExtensionFilter filter = new FileNameExtensionFilter(
		    "Text files", "txt");
		chooser.setFileFilter(filter);
		int returnVal = chooser.showDialog(frmKeywordInContext, "Select Title(s) file");
		
		if(returnVal == JFileChooser.APPROVE_OPTION) {
			lblInputTitlesFilePath.setText(chooser.getSelectedFile().getName());
			lblInputTitlesFilePath.setToolTipText(chooser.getSelectedFile().getAbsolutePath());
			if(checkIfBothInputFilesSpecified()){
				btnProcessInput.setEnabled(true);
			}
			
			// set the content of titles text area
        	titleList = inputProcessorPipe.getTitles(chooser.getSelectedFile().getAbsolutePath());
        	
        	textAreaInputTitles.setText("");
        	
        	try {
        		//set to wait cursor and disable the button
        		frmKeywordInContext.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        		btnSelectTitles.setEnabled(false);
        		btnSelectIgnoreWords.setEnabled(false);
        		
    			// append to the title list
        		for(String title : titleList){
	        		textAreaInputTitles.append(title + "\n");
	        	}
        		textAreaInputTitles.setCaretPosition(0);
        		
        		// Update the number of titles display on UI
        		lblNumOfTitlesDisplay.setText(titleList.size() + "");
	        		
        	} finally {
        		frmKeywordInContext.setCursor(Cursor.getDefaultCursor());
        		btnSelectTitles.setEnabled(true);
        		btnSelectIgnoreWords.setEnabled(true);
        	}
		}
	}
	
	private void loadIgnoreWordsFromFile(){
		chooser.setCurrentDirectory(new File("."));
		FileNameExtensionFilter filter = new FileNameExtensionFilter(
		    "Text files", "txt");
		chooser.setFileFilter(filter);
		int returnVal = chooser.showDialog(frmKeywordInContext, "Select Ignore Word(s) file");
		
		if(returnVal == JFileChooser.APPROVE_OPTION) {
			lblInputIgnoreWordsFilePath.setText(chooser.getSelectedFile().getName());
			lblInputIgnoreWordsFilePath.setToolTipText(chooser.getSelectedFile().getAbsolutePath());
			if(checkIfBothInputFilesSpecified()){
				btnProcessInput.setEnabled(true);
			}
			
			// set the content of titles text area
        	ignoreList = inputProcessorPipe.getIgnoreWords(chooser.getSelectedFile().getName());

        	textAreaInputIgnoreWords.setText("");        	
        	try {
        		//set to wait cursor and disable the button
        		frmKeywordInContext.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        		btnSelectTitles.setEnabled(false);
        		btnSelectIgnoreWords.setEnabled(false);
        		
    			// append to the title list
        		for(String title : ignoreList){
        			textAreaInputIgnoreWords.append(title + "\n");
        			textAreaInputIgnoreWords.repaint();
	        	}
	        	
        		textAreaInputIgnoreWords.setCaretPosition(0);
        		
        		// Update the number of titles display on UI
        		lblNumOfIgnoreWordsDisplay.setText(ignoreList.size() + "");
	        		
        	} finally {
        		frmKeywordInContext.setCursor(Cursor.getDefaultCursor());
        		btnSelectTitles.setEnabled(true);
        		btnSelectIgnoreWords.setEnabled(true);
        	}
		}
	}
	
	private void saveAs()
	{
		JFileChooser fileChooser = new JFileChooser();
    	fileChooser.setDialogTitle("Specify a file to save");   
    	FileNameExtensionFilter filter = new FileNameExtensionFilter(
    		    "Text files", "txt");
    	fileChooser.setFileFilter(filter);
    	fileChooser.setCurrentDirectory(new File("."));
    	int userSelection = fileChooser.showSaveDialog(frmKeywordInContext);
    	 
    	if (userSelection == JFileChooser.APPROVE_OPTION) {
    	    File fileToSave = fileChooser.getSelectedFile();
    	    FileWriter writer;
			try {
				writer = new FileWriter(fileToSave);
				writer.write(textAreaResultOutput.getText().trim());
        	    writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			} 
    	}
	}
	
	private void processInputs(){
		try {
    		//set to wait cursor and disable the button
    		frmKeywordInContext.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
    		btnSelectTitles.setEnabled(false);
    		btnSelectIgnoreWords.setEnabled(false);
    		btnProcessInput.setEnabled(false);
    		lblOuputTitleFilePath.setText(lblInputTitlesFilePath.getText());
    		lblOutputIgnoreWordFilePath.setText(lblInputIgnoreWordsFilePath.getText());
    		
    		//update the start time
        	startTime = System.currentTimeMillis();
        	
    		inputProcessorPipe.syncData(titleList, ignoreList);	
    	} finally {
    		frmKeywordInContext.setCursor(Cursor.getDefaultCursor());
    		btnSelectTitles.setEnabled(true);
    		btnSelectIgnoreWords.setEnabled(true);
    		btnProcessInput.setEnabled(true);
    	}
	}
	
	private boolean checkIfBothInputFilesSpecified()
	{
		// only enable "Process Input" button if both titles and ignore words files are loaded
		if((!lblInputTitlesFilePath.getText().equalsIgnoreCase("not specified")) && (!lblInputIgnoreWordsFilePath.getText().equalsIgnoreCase("not specified"))){
			return true;
		}
		else
			return false;
	}
	
	public static void updateResultOutputDisplay(ArrayList<String> titles)
	{
		textAreaResultOutput.setText("");
		
    	try {
    		//set to wait cursor and disable the button
    		frmKeywordInContext.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
    		
			// append to the title list
    		for(String title : titles){
    			textAreaResultOutput.append(title + "\n");
    			textAreaResultOutput.repaint();
        	}
        	
    		textAreaResultOutput.setCaretPosition(0);
    		
    		// Update the number of titles display on UI
    		lblResultNumOfTitlesDisplay.setText(titles.size() + "");
    		
    		if(titles.size() > 0)
    		{
    			btnSaveAs.setEnabled(true);
    		}
        		
    	} finally {
    		frmKeywordInContext.setCursor(Cursor.getDefaultCursor());
    	}
    	
    	final long endTime = System.currentTimeMillis();
	    lblStatusDisplay.setText("Total time taken: " +(endTime - startTime) +"ms");
	}
}
