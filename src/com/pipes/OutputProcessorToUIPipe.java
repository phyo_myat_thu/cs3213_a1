package com.pipes;

import java.util.ArrayList;

import com.ui.Frm_Main;

public class OutputProcessorToUIPipe implements IPipe {
	@Override
	public void syncData(ArrayList<String> titles,
			ArrayList<String> wordsToIgnore) {
		Frm_Main.updateResultOutputDisplay(titles);
	}
}
