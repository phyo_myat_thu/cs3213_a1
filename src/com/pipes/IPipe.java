package com.pipes;

import java.util.ArrayList;

public interface IPipe {
	public void syncData(ArrayList<String> titles, ArrayList<String> wordsToIgnore );
}
