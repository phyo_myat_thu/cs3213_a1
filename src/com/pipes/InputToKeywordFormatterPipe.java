package com.pipes;

import java.util.ArrayList;

import com.filters.KeywordFormatter;

public class InputToKeywordFormatterPipe implements IPipe {
	
	public void syncData(ArrayList<String> titles, ArrayList<String> wordsToIgnore){
		KeywordFormatter keywordFormatter = new KeywordFormatter(titles, wordsToIgnore);
		keywordFormatter.startProcess();
	}
}
