package com.pipes;

import java.util.ArrayList;
import com.filters.InputProcessor;
import com.filters.KeywordFormatter;

public class UIToInputProcessorPipe implements IPipe {
	
	private String titleSourcePath;
	private String wordstoignoreSourcePath;
	private ArrayList<String> titleList;
	private ArrayList<String> ignoreList;
	
	/* Constructor
	 * @param String titleSourcePath
	 * 		Path to the text file containing titles
	 * @param String wordstoignoreSourcePath
	 * 		Path to the text file containing words to be ignored
	 * 
	 */
	public UIToInputProcessorPipe(String titleSourcePath, String wordstoignoreSourcePath) {
		this.titleSourcePath = titleSourcePath;
		this.wordstoignoreSourcePath = wordstoignoreSourcePath;
	}
	
	public ArrayList<String>getTitles(String pathToTitles)
	{
		InputProcessor inputProcessor = new InputProcessor(titleSourcePath, wordstoignoreSourcePath);
		this.titleList = inputProcessor.getTitles(pathToTitles);
		return titleList;
	}
	
	public ArrayList<String>getIgnoreWords(String pathToIgnoreList)
	{
		InputProcessor inputProcessor = new InputProcessor(titleSourcePath, wordstoignoreSourcePath);
		this.ignoreList = inputProcessor.getTitles(pathToIgnoreList);
		return this.ignoreList;
	}
	
	@Override
	public void syncData(ArrayList<String> titles, ArrayList<String> wordsToIgnore){
		if(titles != null && wordsToIgnore != null){
			KeywordFormatter keywordFormatter = new KeywordFormatter(titles, wordsToIgnore);
			keywordFormatter.startProcess();
		}
		else{
			KeywordFormatter keywordFormatter = new KeywordFormatter(titleList, ignoreList);
			keywordFormatter.startProcess();
		}
	}
}
