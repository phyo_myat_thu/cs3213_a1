package com.pipes;

import java.util.ArrayList;

import com.filters.OutputProcessor;

public class AlphabetizerToOutputPipe implements IPipe{

	public void syncData(ArrayList<String> titles,
			ArrayList<String> wordsToIgnore) {
		OutputProcessor outputProc = new OutputProcessor(titles, wordsToIgnore);
		outputProc.startProcess();
	}
}
