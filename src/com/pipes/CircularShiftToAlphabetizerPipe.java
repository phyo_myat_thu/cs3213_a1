package com.pipes;

import java.util.ArrayList;

import com.filters.Alphabetizer;

public class CircularShiftToAlphabetizerPipe implements IPipe{

	@Override
	public void syncData(ArrayList<String> titles,
			ArrayList<String> wordsToIgnore) {
		Alphabetizer alphabetizer = new Alphabetizer(titles, wordsToIgnore);
		alphabetizer.startProcess();
	}
}
