package com.pipes;

import java.util.ArrayList;
import com.filters.CircularShift;

public class KeywordFormatterToShiftPipe implements IPipe {

	@Override
	public void syncData(ArrayList<String> titles,
			ArrayList<String> wordsToIgnore) {
		CircularShift circularShift = new CircularShift(titles, wordsToIgnore);
		circularShift.startProcess();
	}

}
