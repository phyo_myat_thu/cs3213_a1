/*
 * IFilter.java
 * Interface to which all the filters must implements
 */

package com.filters;

public interface IFilter {
	public void startProcess();
}
