/*
 * Alphabetizer.java
 * Handles sorting of list according to alphabets
 * Author: Saloni Kaur
 */

package com.filters;
import java.util.ArrayList;
import java.util.HashSet;
import java.lang.String;

import com.pipes.CircularShiftToAlphabetizerPipe;

public class CircularShift implements IFilter {
	private ArrayList<String> titleList;
	private ArrayList<String> ignoreList;
	private ArrayList<String> newList;
	private ArrayList<String> resultList;
	
	public CircularShift(ArrayList<String> titles, ArrayList<String> wordsToIgnore){
		titleList = titles;
		ignoreList = wordsToIgnore;
	}
	
	private void processShifting() {
		//create new resultant arrayList
		newList = new ArrayList<String>();
		resultList = new ArrayList<String>();
		
		//iterate through every title
		for (int x = 0; x < titleList.size(); x++){
			String[] titleArray = ((String) titleList.get(x)).split("\\s+");
			
			ArrayList<String> titleArrList = convertToArrList(titleArray); 
			
			HashSet<String> ignoreWordsSet = new HashSet<String>();
			
			for(String ignoreWord : ignoreList){
				ignoreWordsSet.add(ignoreWord);
			}
			
			//for movie titles with length of 1 word, no circular shift is required
			if(titleArrList.size() == 1){
				
					// But need to still check if that 1 word is in the ignore word list or not
					if(!ignoreWordsSet.contains(titleArrList.get(0))){
						addToResult(titleArrList);
					}
			} 
			// If the title is more than 1 word, do the following:
			else {
				for (int y = 0; y < titleArrList.size(); y++){
					
					ArrayList<String> resultString = doCircularShift(titleArrList);
					titleArrList = resultString;
					if(!ignoreWordsSet.contains(titleArrList.get(0))){
						addToResult(titleArrList);
					}
				}
			}
		}
	}
	
	private void addToResult(ArrayList<String> titleArrList) {
		StringBuilder sb = new StringBuilder();
		for (int x = 0; x < titleArrList.size(); x++){
			
			sb.append(titleArrList.get(x));
			
			if(x < titleArrList.size() - 1){
				sb.append(" ");
			}
		}
		resultList.add(sb.toString());
		
	}

	private ArrayList<String> convertToArrList(String[] titleArray){
		
		ArrayList<String> titleArrList = new ArrayList<String>();

		//convert array to arrayList
		for(int z = 0; z < titleArray.length; z++){
			titleArrList.add(titleArray[z]);
		}
		return titleArrList;
	}

	private  ArrayList<String> doCircularShift(ArrayList<String> title) {
		Object[] safeToModify = (Object[]) title.toArray();
		String wordToShiftA = (String)safeToModify[safeToModify.length-1];
		title.remove(wordToShiftA);
		title.add(0, wordToShiftA);
		return title;
	}

	@Override
	public void startProcess() {
		processShifting();
		CircularShiftToAlphabetizerPipe pipe = new CircularShiftToAlphabetizerPipe();
		pipe.syncData(resultList, ignoreList);
	}

	
}
