/*
 * KeywordFormatter.java
 * Handles Formatting of keyword and words to ignore
 * Author: Phyo Myat Thu
 */
package com.filters;
import java.util.ArrayList;
import java.util.Arrays;
import com.pipes.KeywordFormatterToShiftPipe;


public class KeywordFormatter implements IFilter {
	private ArrayList<String> listOfTitles;
	private ArrayList<String> listOfWordsToIgnore;
	
	public KeywordFormatter(ArrayList<String> listOfTitles, ArrayList<String> listOfWordsToIgnore) {
		this.listOfTitles = listOfTitles;
		this.listOfWordsToIgnore = listOfWordsToIgnore;
	}
	
	private void formatKeyWords()
	{
		String[] arrayOfWordsToIgnore = new String[listOfWordsToIgnore.size()];
		listOfWordsToIgnore.toArray(arrayOfWordsToIgnore);
		
		for(int i = 0 ; i < arrayOfWordsToIgnore.length; i++){
			arrayOfWordsToIgnore[i] = arrayOfWordsToIgnore[i].toLowerCase();
		}
		
		Arrays.sort(arrayOfWordsToIgnore); // Sort the array first so that we can use the binary search to be more efficient
		
		for(int i=0; i< listOfTitles.size(); i++) {
			String title = listOfTitles.get(i).trim();			
			String[] wordsInTitle = title.split("\\s+");
			for(int j=0; j< wordsInTitle.length; j++) {
				String wordInTitle = wordsInTitle[j];

				int index = Arrays.binarySearch(arrayOfWordsToIgnore, wordInTitle.toLowerCase());
				// If it is not a word to ignore
				if(index < 0) {
					// Capitalize first letter and change lower case to the rest
					if(!wordInTitle.trim().equalsIgnoreCase("")){
						wordsInTitle[j] = wordInTitle.substring(0, 1).toUpperCase() + wordInTitle.substring(1).toLowerCase();
					}
				}
				else{
					wordsInTitle[j] = wordInTitle.toLowerCase();
				}
			}
						
			StringBuilder builder = new StringBuilder();
			for(String s : wordsInTitle) {
			    builder.append(s);
			    builder.append(" ");
			}
			listOfTitles.set(i, builder.toString().trim());
			listOfWordsToIgnore = new ArrayList<String>(Arrays.asList(arrayOfWordsToIgnore));
		}
		
	}
	
	@Override
	/* Communicates with neighbor pipe to pass the data and kick start the processing
	 * 
	 */
	public void startProcess() {
		formatKeyWords();
		KeywordFormatterToShiftPipe pipe = new KeywordFormatterToShiftPipe();
		pipe.syncData(listOfTitles, listOfWordsToIgnore);
	}
}
