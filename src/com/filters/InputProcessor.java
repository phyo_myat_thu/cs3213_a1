/*
 * InputProcessor.java
 * Handles reading and parsing of input data from text file
 * Author: Phyo Myat Thu
 */

package com.filters;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import com.pipes.InputToKeywordFormatterPipe;

public class InputProcessor implements IFilter {
	private String titleSourcePath;
	private String wordstoignoreSourcePath;
	private ArrayList<String> titleList;
	private ArrayList<String> ignoreList;
	
	/* Constructor
	 * @param String titleSourcePath
	 * 		Path to the text file containing titles
	 * @param String wordstoignoreSourcePath
	 * 		Path to the text file containing words to be ignored
	 * 
	 */
	public InputProcessor(String titleSourcePath, String wordstoignoreSourcePath) {
		this.titleSourcePath = titleSourcePath;
		this.wordstoignoreSourcePath = wordstoignoreSourcePath;
	}
	
	public InputProcessor() {
		this.titleSourcePath = "";
		this.wordstoignoreSourcePath = "";
	}
	
	/* Get the list of titles
	 * @return ArrayList of titles 
	 * 
	 */
	public ArrayList<String> getTitles() {
		titleList = getListItems(titleSourcePath);
		return titleList;
	}
	
	/* Get the list of titles
	 * @return ArrayList of titles 
	 * 
	 */
	public ArrayList<String> getTitles(String titleSourcePath) {
		titleList = getListItems(titleSourcePath);
		return titleList;
	}
	
	/* Get the list of words to ignore
	 * @return ArrayList of words to ignore 
	 * 
	 */
	public ArrayList<String> getWordsToIgnore() {
		ignoreList = getListItems(wordstoignoreSourcePath);
		return ignoreList;
	}
	
	/* Get the list of words to ignore
	 * @return ArrayList of words to ignore 
	 * 
	 */
	public ArrayList<String> getWordsToIgnore(String wordstoignoreSourcePath) {
		ignoreList = getListItems(wordstoignoreSourcePath);
		return ignoreList;
	}
	
	/* Handles file I/O and reading of data from text files
	 * @return ArrayList of lines in the file
	 * 
	 */
	private ArrayList<String> getListItems(String fileSourcePath)
	{
		ArrayList<String> listItems = new ArrayList<String>();
		
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(fileSourcePath));
		
		String inputLine;
		while ((inputLine = br.readLine()) != null) {
			listItems.add(inputLine.trim());
		}
		br.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return listItems;
	
	}

	@Override
	/* Communicates with neighbor pipe to pass the data and kick start the processing
	 * 
	 */
	public void startProcess() {
		InputToKeywordFormatterPipe pipe = new InputToKeywordFormatterPipe();
		
		if(titleList != null && ignoreList != null){
			pipe.syncData(getTitles(), getWordsToIgnore());
		}
		else if(!titleSourcePath.equalsIgnoreCase("") &&  !wordstoignoreSourcePath.equalsIgnoreCase("")){
			pipe.syncData(titleList, ignoreList);
		}
	}
}
