/*
 * Alphabetizer.java
 * Handles sorting of list according to alphabets
 * Author: Phyo Myat Thu
 */
package com.filters;
import java.util.ArrayList;
import java.util.Collections;
import com.pipes.AlphabetizerToOutputPipe;


public class Alphabetizer implements IFilter {
	private ArrayList<String> titleList;
	private ArrayList<String> ignoreList;
	
	public Alphabetizer(ArrayList<String> titles, ArrayList<String> wordsToIgnore){
		titleList = titles;
		ignoreList = wordsToIgnore;
	}
	
	private void sortTitlesAlphabatically() {
		Collections.sort(titleList);	
	}

	public void startProcess() {
		sortTitlesAlphabatically();
		AlphabetizerToOutputPipe pipe = new AlphabetizerToOutputPipe();
		pipe.syncData(titleList, ignoreList);
	}
}
