package com.filters;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;

import com.pipes.OutputProcessorToUIPipe;

public class OutputProcessor implements IFilter {
	private ArrayList<String> titleList;
	private ArrayList<String> ignoreList;
	
	public OutputProcessor(ArrayList<String> titles, ArrayList<String> wordsToIgnore){
		titleList = titles;
		ignoreList = wordsToIgnore;
	}
	
	private void save(String fileName) throws FileNotFoundException {
	    PrintWriter pw = new PrintWriter(new FileOutputStream(fileName));
	    for (String title : titleList)
	        pw.println(title);
	    pw.close();
	}
	
	@Override
	/* Communicates with neighbor pipe to pass the data and kick start the processing
	 * 
	 */
	public void startProcess() {
		try {
			//save to output file
			save("output.txt");
			
			//call output pipe to communicate to update the UI
			OutputProcessorToUIPipe toUpdateUI = new OutputProcessorToUIPipe();
			toUpdateUI.syncData(titleList, ignoreList);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
}
